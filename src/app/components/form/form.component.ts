import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
  formulario!: FormGroup;
  contenido2: string[] = [];

  constructor(private fb: FormBuilder) {
    this.crearFormulario();
  }

  crearFormulario(): void {
    this.formulario = this.fb.group({
      cajaG: [''],
      cajaP: this.fb.array([[]]),
    });
  }

  ngOnInit(): void {}

  get contenidoP() {
    return this.formulario.get('cajaP') as FormArray;
  }

  agregar(): void {
    this.contenidoP.push(this.fb.control('', Validators.required));
  }

  borrar(i: number): void {
    this.contenidoP.removeAt(i);
  }

  limpiar(): void {
    this.contenido2 = [''];
    this.formulario.reset;
  }
  limpiarMultilinea(): void {
    this.contenido2 = [''];
  }

  guardar(): void {
    this.contenido2 = this.formulario.value.cajaP;
  }
}
